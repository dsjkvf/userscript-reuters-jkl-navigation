// ==UserScript==
// @name        reuters: j/k/v navigation
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-reuters-jkl-navigation
// @downloadURL https://bitbucket.org/dsjkvf/userscript-reuters-jkl-navigation/raw/master/rjkl.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-reuters-jkl-navigation/raw/master/rjkl.user.js
// @include     https://www.reuters.com/theWire*
// @run-at      document-start
// @version     1.03
// @grant       none
// ==/UserScript==

/*
    This userscript was originally introduced here:
        https://greasyfork.org/en/forum/discussion/41801/scrolling-page-by-class
    by wOxxOm:
        https://greasyfork.org/en/forum/profile/wOxxOm
    Thus, all the credits should go directly to the original author.

    The small hack allowing to open the current header in a new tab was added
    by the current script maintainer dsjkvf <dsjkvf@gmail.com>.

    None websites were harmed during this dissection.
*/

addEventListener('keyup', function(e) {
    if (e.ctrlKey ||
        e.altKey ||
        e.metaKey ||
        e.shiftKey ||
        (e.which !== 74 /*j*/ && e.which !== 75 /*k*/ && e.which !== 76 /*v*/ )) {
        return;
    }
    e.preventDefault();

    var MARGIN_TOP = 50;
    var direction = e.which === 74 ? 1 : -1;
    // collect all the available H2 headers
    var elements = document.getElementsByTagName('h2');
    var len = elements.length;
    var offsets = [];
    // extract an offset and an URL of a particular header; return as a list
    var getOffset = function (i) {
        var myRegex = /.*(http.+?)".*/;
        return offsets[i] || ((offsets[i] = [elements[i].getBoundingClientRect().top, elements[i].innerHTML.replace(myRegex, "$1")]));
    };
    var a = 0, b = len - 1;
    while (a < b - 1) {
        var c = (a + b) / 2 | 0;
        if (getOffset(c)[0] < 0) {
            a = c;
        } else {
            b = c;
        }
    }
    while (a < len && getOffset(a)[0] < direction) {
        a++;
    }

    // detect user's input:
    // if it's a directional command, j or k, then scroll the correposnding header up to the top
    if (e.which !== 76) {
    a = Math.max(0, Math.min(a + direction, len));
    (function () {
        try {
            scrollTo({
                top: scrollY + getOffset(a)[0] - MARGIN_TOP,
                behavior: 'smooth',
            });
        } catch (_) {
            scrollTo(scrollX, scrollY + getOffset(a)[0] - MARGIN_TOP);
        }
    })();
    // otherwise, if it's a navigational command, v, then open the correposnding page in a new tab or window
    } else {
        window.open(getOffset(a)[1], '_blank');
    }
});
