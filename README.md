Reuters: j/k/v naigation
========================

## About

### Disclaimer

As of 2019-01-01, the scripts included in this repository, are no longer maintained. Instead, these scripts are experimental (in the *proof-of-concept* sense) and unsupported.

### Description

This is a small [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting Reuters' [The Wire](https://www.reuters.com/theWire) page -- will add a basic Vim-like `j/k` scrolling navigation between articles with level 2 heading markup, and will also open a current article in a new tab on `l` key press.

## Versions

The original [script](https://bitbucket.org/dsjkvf/userscript-reuters-jkl-navigation/raw/master/rjkl.user.js) was [written](https://greasyfork.org/en/forum/discussion/41795/scrolling-page-by-class) by [wOxxOm](https://greasyfork.org/en/forum/profile/wOxxOm) and then only extended (in some clumsy way) to allow a user to open the currently selected article in a new tab. Therefore, naturally, all the credits go to the original author.

*It's also worth noting that this Reuters implementation is just a special case of wOxxOm's `j/k` navigation algorithm, and the script can easily be adapted for other sites and attributes.*

However, the repo also contains another take on the task: [the script](https://bitbucket.org/dsjkvf/userscript-reuters-jkl-navigation/raw/master/rjkl-re-done.user.js) `rjkl-re-done.user.js`, which doesn't use any of wOxxOm's code but instead utilizes another way of creating a table of offsets needed for navigation around articles (and is also free from a nasty bug that starts scrolling from the second article instead of the first one).

It's also worth noting that these both versions are kept in order to allow users to decide, which exactly solution will suit them best -- as well as for some educational purposes, too.
