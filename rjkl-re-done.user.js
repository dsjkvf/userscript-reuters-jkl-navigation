// ==UserScript==
// @name        reuters - j/k/l navigation (re-done)
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-reuters-jkl-navigation
// @downloadURL https://bitbucket.org/dsjkvf/userscript-reuters-jkl-navigation/raw/master/rjkl-re-done.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-reuters-jkl-navigation/raw/master/rjkl-re-done.user.js
// @include     https://www.reuters.com/theWire*
// @version     1.0.3
// @run-at      document-end
// @grant       none
// ==/UserScript==

// INIT

// Add a bit of styling (you don't really need this)
var head = document.getElementsByClassName('FeedPage_title-wrapper');
head[0].style.fontStyle = "italic";

// Init the main variables
var curPos = 0,
    articleDetails = [],
    offsets = [];

// HELPERS

function fillOffsets() {
    /* removes crap, fills the offsets table
    */
    scrollTo(0, 0);

    var crap = document.getElementsByClassName('FeedItem_dp-slot-container');
    var i = 0;
    while (i < crap.length) {
        crap[i].style.display = "none";
        i++;
    };

    var elements = document.getElementsByClassName('FeedItemHeadline_headline');
    function getOffset(i) {
        var myRegex = /.*(http.+?)".*/;
        return offsets[i] || ((offsets[i] = [elements[i].getBoundingClientRect().top, elements[i].innerHTML.replace(myRegex, "$1")]));
    };
    i = 0;
    while (i < elements.length) {
        getOffset(i);
        i++;
    };
}

function getPos(el, arrayElements) {
    /* returns a pair of articles that surround the current scroll position
    */

    var i = 0;
    if (el < arrayElements[0][0]) {
        return [0, arrayElements[0]];
    }
    while (i < arrayElements.length) {
        if (arrayElements[i][0] < el && el <= arrayElements[i+1][0]) {
            return [arrayElements[i], arrayElements[i+1]];
        };
        i++;
    };
};

function toArticle(scroll, down, index, delta) {
    /* scrolls (true), or opens the selected article in a new tab (false);
       scrolls either down (true) or up (false);
       takes coordinates from either lower, or upper neighboring article;
       adds either positive, or negative delta (for scrolling purposes)
    */

    curPos = document.body.scrollTop + delta;
    if (down) {
        offsets = [];
        fillOffsets();
    };
    articleDetails = getPos(curPos, offsets);
    if (scroll) {
        scrollTo(0, articleDetails[index][0] - 1);
    } else {
        window.open(articleDetails[index][1], '_blank');
    }
}

// MAIN

addEventListener('keyup', function(e) {
    if (e.ctrlKey ||
        e.altKey ||
        e.metaKey ||
        e.shiftKey ||
        (e.which !== 74 /*j*/ && e.which !== 75 /*k*/ && e.which !== 76 /*l*/ && e.which !== 88 /*x*/ )) {
        return;
    }
    e.preventDefault();

    if (e.which == 88) {
        // doesn't really scroll or open, just re-fills the offsets table
        toArticle(true, true, 1, -2);
    } else if (e.which == 74) {
        // scrolls down, re-fills the offsets table
        toArticle(true, true, 1, 2);
    } else if (e.which == 75) {
        // scrolls up, doesn't re-fill the offsets table
        toArticle(true, false, 0, -2);
    } else if (e.which == 76) {
        // opens the current article in a new tab, sure doesn't re-fill the offsets table
        toArticle(false, false, 1, 0);
    }
});
